const my_array_alpha = (str) => {

    let len = 0;
    let arr = [];

    while(str[len]){
        arr.push(str[len]);
        len++;
    }

    return arr;
}

module.exports = my_array_alpha;