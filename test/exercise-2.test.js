const my_display_alpha_reverse = require('../exercise-2');
const assert = require('assert');

describe("my_display_alpha_reverse", () => {

    it("should return all the letters of the alphabet from z to a", () => {
        assert.equal(my_display_alpha_reverse(), "zyxwvutsrqponmlkjihgfedcba");
    });

});