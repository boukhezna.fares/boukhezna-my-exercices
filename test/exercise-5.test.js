const my_array_alpha = require('../exercise-5');
const assert = require('assert');

describe("my_array_alpha", () => {

    it("should return all char from string", () => {
        assert.deepEqual(my_array_alpha("aaa"), ['a', 'a', 'a']);
    });

});