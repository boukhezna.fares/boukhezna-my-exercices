const my_display_alpha = require('../exercise-1');
const assert = require('assert');

describe("my_display_alpha", () => {

    it("should return all the letters of the alphabet from a to z", () => {
        assert.equal(my_display_alpha(), "abcdefghijklmnopqrstuvwxyz");
    });

});