const  my_alpha_number = require('../exercise-3');
const assert = require('assert');

describe("my_alpha_number", () => {

    it("should return the number in string format", () => {
        assert.equal(my_alpha_number(4), "4");
    });

});