const my_size_alpha = require('../exercise-4');
const assert = require('assert');

describe("my_size_alpha", () => {

    it("should return size of the string", () => {
        assert.equal(my_size_alpha("abcd"), 4);
    });

    it("should return 0 if not string", () => {
        assert.equal(my_size_alpha(134), 0);
    });


});