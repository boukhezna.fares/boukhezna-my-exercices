const my_length_arr = require('../exercise-6');
const assert = require('assert');

describe("my_length_arr", () => {

    it("should return the size of the array passed in parameters", () => {
        assert.equal(my_length_arr(['a', 'b', 'c']), 3);
    });

});