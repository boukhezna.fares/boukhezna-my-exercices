const my_sum = require('../exercise-0');
const assert = require('assert');

describe("my_sum", () => {

    it("should return the sum of a and b", () => {
        assert.equal(my_sum(4, 3), 7);
    });

    it("should return 0 si aucune valeur n'est passé en a ou en b", () => {
        assert.equal(my_sum(4), 0);
    });

    it("should return 0 si les arguments passés ne sont pas des nombres", () => {
        assert.equal(my_sum("aaaa", "bbbb"), 0);
    });
});