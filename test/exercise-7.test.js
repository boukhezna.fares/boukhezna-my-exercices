const my_is_posi_neg = require('../exercise-7');
const assert = require('assert');

describe("my_is_posi_neg", () => {

    it("should return positive if positive number", () => {
        assert.equal(my_is_posi_neg(4), "POSITIF");
    });

    it("should return negative if negative number", () => {
        assert.equal(my_is_posi_neg(-5), "NEGATIF");
    });

    it("should return neutral if 0", () => {
        assert.equal(my_is_posi_neg(0), "NEUTRAL");
    });

    it("should return positive if null or undefined", () => {
        assert.equal(my_is_posi_neg(undefined), "POSITIF");
    });
});