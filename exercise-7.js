const my_is_posi_neg = (nbr) => {

    if(nbr == 0){
        return "NEUTRAL";
    }else if(nbr > 0 || !nbr){
        return "POSITIF";
    }else {
        return "NEGATIF";
    }

}

module.exports = my_is_posi_neg;