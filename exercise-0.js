const my_sum = (a , b) => {
    
    if(!a || !b)
        return 0;

    if(typeof a !== "number" || typeof b !== "number")
        return 0;

    return a + b;
}

module.exports = my_sum;