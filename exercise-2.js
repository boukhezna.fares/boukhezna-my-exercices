const my_display_alpha = require('./exercise-1');

const my_display_alpha_reverse = () => {

    let len = 0;
    let str = my_display_alpha();
    let newStr = "";

    while(str[len]){
        len++;
    }

    len--;

    while(len >= 0){
        newStr += str[len];
        len --;
    }

    return newStr;
    
}

module.exports = my_display_alpha_reverse;