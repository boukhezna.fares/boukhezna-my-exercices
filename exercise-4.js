const my_size_alpha = (str) => {

    if (typeof str !== 'string')
        return 0;

    let len=0;

    while(str[len]){
        len++
    }
    
    return len;
}

module.exports = my_size_alpha;